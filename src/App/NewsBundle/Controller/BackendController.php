<?php

namespace App\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BackendController extends Controller
{
    public function adminAction()
    {
        return $this->render('AppNewsBundle:Backend:admin.html.twig', array(
            // ...
        ));
    }

}
