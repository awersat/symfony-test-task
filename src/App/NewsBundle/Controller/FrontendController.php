<?php

namespace App\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontendController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppNewsBundle:Frontend:index.html.twig', array(
            // ...
        ));
    }

}
